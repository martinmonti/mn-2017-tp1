import sys
import os
import random
from operator import itemgetter
from shutil import copyfile


#argv example python cmm_strategy.py <dat file> <number of matches to play> <stragy number>
results_path = './output/experimento/strategy/output_resultado'
output_experiment_path = './output/experimento/strategy/output_experimento'
copy_of_matches_path = './output/experimento/strategy/copy_of_matches'
max_radom_matches_path = './output/experimento/strategy/max_radom_matches'
argv_global = None
def main(argv):
	argv_global = argv
	init()

	#number of matches to try getting the best strategy
	n = int(argv[2])

	#argv 1 has the data 
	dictionary_team_ranking = get_dictionary_from_path(argv[1])	
	new_team_number = max(dictionary_team_ranking.keys()) + 1
	# posibles values are  0 1 2 3 4 
	strategy = int(argv[3])
	# our strategy
	if strategy == 0:
		best = keywithmaxval(dictionary_team_ranking, 0)
		output = relative_best(new_team_number, best, n)
	elif strategy == 1:
		best = keywithmaxval(dictionary_team_ranking, 0)
		output = win_n_times(new_team_number, best, n)
	elif strategy == 2:
		worst = keywithmaxval(dictionary_team_ranking, 1)
		output = win_n_times(new_team_number, worst, n)
	elif strategy == 3:
		relative = keywithmaxval(dictionary_team_ranking, 2)
		output = relative_above(new_team_number, relative, n)
	elif strategy == 4:
		N = 1000
		output = max_from_random_N_times(new_team_number, dictionary_team_ranking.keys(), N, n)
	output_experimento = open(output_experiment_path, 'w')




	output_experimento.write('\n'.join([str(i) for i in output]))	
	output_experimento.close()

#win n the same team
def win_n_times(new_team, team, n):
	result = []
	for n in range(0, n):
		matches = open(copy_of_matches_path, 'r')
		first_line = matches.readline().strip('\n').split(' ')
		first_line[1] = str(int(first_line[1]) + 1)
		first_line = (' ').join(first_line)
		cmd = "sed -i '' \"1s/.*/" + first_line + "/\" " + copy_of_matches_path
		#print cmd
		os.system(cmd)
		matches.close()
		matches = open(copy_of_matches_path, 'a')
		new_match = '1 ' + str(new_team) +' 1 ' +  str(team) + ' 0' +'\n'
		matches.write(new_match)
		#print new_match
		matches.close()
		dictionary_team_ranking = get_dictionary_from_path(copy_of_matches_path)
		sorted(dictionary_team_ranking.items(), key=itemgetter(0))
		result.append(dictionary_team_ranking[new_team])	
	return result	


def relative_best(new_team, team, n):
	result = []
	for n in range(0, n):
		matches = open(copy_of_matches_path, 'r')
		first_line = matches.readline().strip('\n').split(' ')
		first_line[1] = str(int(first_line[1]) + 1)
		first_line = (' ').join(first_line)
		cmd = "sed -i '' \"1s/.*/" + first_line + "/\" " + copy_of_matches_path
		#print cmd
		os.system(cmd)
		matches.close()
		matches = open(copy_of_matches_path, 'a')
		new_match = '1 ' + str(new_team) +' 1 ' +  str(team) + ' 0' +'\n'
		matches.write(new_match)
		#print new_match
		matches.close()
		dictionary_team_ranking = get_dictionary_from_path(copy_of_matches_path)
		result.append(dictionary_team_ranking[new_team])
		dictionary_team_ranking[new_team] = None
		team = keywithmaxval(dictionary_team_ranking, 0)
	return result

def max_from_random_N_times(new_team, keys, N, n):
	max_list = None
	for x in range(0, 100):
		result = []
		random.shuffle(keys)
		for i in keys[0:n]:
			matches = open(copy_of_matches_path, 'r')
			first_line = matches.readline().strip('\n').split(' ')
			first_line[1] = str(int(first_line[1]) + 1)
			first_line = (' ').join(first_line)
			cmd = "sed -i '' \"1s/.*/" + first_line + "/\" " + copy_of_matches_path
			#print cmd
			os.system(cmd)
			matches.close()
			matches = open(copy_of_matches_path, 'a')
			new_match = '1 ' + str(new_team) +' 1 ' +  str(i) + ' 0' +'\n'
			matches.write(new_match)
			#print new_match
			matches.close()
			dictionary_team_ranking = get_dictionary_from_path(copy_of_matches_path)
			result.append(dictionary_team_ranking[new_team])
		if max_list is None:
			print 'entra una sola vez'
			max_list = result
			copyfile(copy_of_matches_path, max_radom_matches_path)
		else :
			print result
			print max_list[n-1]
			print result[n-1]
			if max_list[n-1] < result[n-1]:
				max_list = result
				copyfile(copy_of_matches_path, max_radom_matches_path)
		init()	
	return max_list

def relative_above(new_team, team, n):
	result = []
	for n in range(0, n):
		matches = open(copy_of_matches_path, 'r')
		first_line = matches.readline().strip('\n').split(' ')
		first_line[1] = str(int(first_line[1]) + 1)
		first_line = (' ').join(first_line)
		cmd = "sed -i '' \"1s/.*/" + first_line + "/\" " + copy_of_matches_path
		#print cmd
		os.system(cmd)
		matches.close()
		matches = open(copy_of_matches_path, 'a')
		new_match = '1 ' + str(new_team) +' 1 ' +  str(team) + ' 0' +'\n'
		matches.write(new_match)
		#print new_match
		matches.close()
		dictionary_team_ranking = get_dictionary_from_path(copy_of_matches_path)
		ranking = dictionary_team_ranking[new_team]
		#print ranking
		result.append(ranking)
		dictionary_team_ranking[new_team] = None
		#print ranking
		team = keywithmaxval(dictionary_team_ranking, 2, ranking)
	return result


def get_dictionary_team_ranking(lines):
	dic = {}
	for line in lines:
		parts = line.strip('\n').split(' ')
		dic[int(parts[0])] = float(parts[1])
	return dic	

def get_dictionary_from_path(path):
	cmd = './tp1 ' + path + ' ' + results_path + ' 0'
	os.system(cmd)
	#print cmd
	results = open(results_path, 'r')
	output_experiment = open(output_experiment_path, 'w')
	dictionary_team_ranking = get_dictionary_team_ranking(results.readlines())
	results.close()
	return dictionary_team_ranking

def init():
	copyfile(sys.argv[1], copy_of_matches_path)
	matches = open(copy_of_matches_path , 'r')
	first_line = matches.readline().strip('\n').split(' ')
	first_line[0] = str(int(first_line[0]) + 1)
	first_line = (' ').join(first_line)
	matches.close()
	cmd = "sed -i '' \"1s/.*/" + first_line + "/\" " + copy_of_matches_path 
	os.system(cmd)

def minimum(x):
    mini = x[0]
    for i in x[0:]:
        if i < mini:
            mini = i
    return mini

#get the key of the max or the min value on a dictionary
def keywithmaxval(d, mode, ranking=0.5): 
	v=list(d.values())
	k=list(d.keys())
	if mode == 0:
		return k[v.index(max(v))]
	elif mode == 1:
		return k[v.index(minimum(v))]
	elif mode == 2:
		min = v[0]
		for vi in v:
			if vi is not None:
				dif = abs(vi - ranking)
				ant_dif = abs(min - ranking)
				if dif < ant_dif:
					min = vi  
		return k[v.index(min)]	
	else: return None

if __name__ == "__main__":
    main(sys.argv)