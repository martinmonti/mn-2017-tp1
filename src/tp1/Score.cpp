#include "Score.h"
#include <fstream>
#include <cstdlib>
#include <vector>
#include <set>

Score::Score(){
}

Score::~Score(){
}

int Score::getTeams() {
	return teams;
}

void Score::setTeams(int n_teams){
	teams = n_teams;
}

int Score::getGames() {
	return games;
}
    
void Score::setGames(int n_games){
	games = n_games;
}

vector<Game> Score::getMatches() {
	return _matches;
}
    
void Score::setMatches(vector<Game> matches){
	_matches.swap(matches);
}

vector<int> Score::getOrderTeams(){
    return _order_teams;
}

void Score::readInputFile(char* fileName){
		
		ifstream inputFile;
		string line; 
		string delimiter = " ";
		size_t pos = 0;
        set<int> order_teams;
    
    	inputFile.open(fileName);
    	if (inputFile.is_open()) {
			getline(inputFile,line);
			
			size_t pos = line.find(delimiter);
			int n_teams = atoi(line.substr(0, pos).c_str());
			int n_games = atoi(line.substr(pos+1).c_str());
			teams = n_teams;
			games = n_games;
			
			while (! inputFile.eof() ) {
            	
            	Game game;
            	
				getline (inputFile,line);
				
				if(!line.empty()){
				
					pos = line.find(delimiter);
            		string date = line.substr(0, pos);
            		game.setDate(date);
            		line = line.substr(pos+1);
            	
					pos = line.find(delimiter);
            		int teamA = atoi(line.substr(0, pos).c_str());
            		game.setTeamA(teamA);
                	order_teams.insert(teamA);
            		line = line.substr(pos+1);
            	
            		pos = line.find(delimiter);
            		int goalsA = atoi(line.substr(0, pos).c_str());
            		game.setGoalsA(goalsA);
            		line = line.substr(pos+1);
            	
            		pos = line.find(delimiter);
            		int teamB = atoi(line.substr(0, pos).c_str());
            		game.setTeamB(teamB);
                	order_teams.insert(teamB);                            
            		line = line.substr(pos+1);
            	
            		pos = line.find(delimiter);
            		int goalsB = atoi(line.substr(0, pos).c_str());
            		game.setGoalsB(goalsB);
            		line = line.substr(pos+1);
            	
					_matches.push_back(game);
				}
        	}
        	inputFile.close();
            copy(order_teams.begin(), order_teams.end(), inserter(_order_teams, _order_teams.begin()));
    	} else {
			cout << "File: " << fileName << " could not be opened " << endl; 
		}
}

int Score::getIndex(int team){
    for(int i=0; i < _order_teams.size() ;i++){
        if(_order_teams[i] == team){
            return i;        
        }
    }
    return -1;
}

void Score::simulateCompetition(int n_teams, int games){
    /* initialize random seed: */
    srand (time(NULL));    
    
    setTeams(n_teams);
    setGames(games);
    _order_teams.clear();
    for(int i = 0; i < n_teams; i++){
        _order_teams.push_back(i);
    }
    for(int i = 0; i < games; i++){
        Game game;
        game.setDate("1");
        int teamA = rand() % teams;
        int teamB = rand() % teams;        
        int goalsA = rand() % 20;
        int goalsB = rand() % 20;        

        game.setTeamA(teamA);
        game.setGoalsA(goalsA);
        if(teamA != teamB){
            game.setTeamB(teamB);
        }else{
            if(teamB < (teams-1)){
                teamB++;            
            }else{
                teamB--;
            }
            game.setTeamB(teamB);
        }
        if(goalsA != goalsB){
            game.setGoalsB(goalsB);
        }else{
            goalsB++;
            game.setGoalsB(goalsB);
        }
        _matches.push_back(game);
   }
}

