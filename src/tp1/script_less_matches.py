import sys
import os

file = open(sys.argv[1], 'r')
map = {}
file.readline()
for line in file.readlines():
	split_lines = line.strip('\n').split(' ')
	key = split_lines[1]
	if key in map:
		map[key] += 1
	else :
		map[key] = 1
	key = split_lines[3]
	if key in map:
		map[key] += 1
	else :
		map[key] = 1
print min(map, key=map.get)		