#ifndef Score_H
#define Score_H

#include <vector>
#include <set>
#include "Game.h"

using namespace std;

class Score {
	
	private:
    
    int teams;
    int games;
    vector<Game> _matches;
    vector<int> _order_teams;
    
    public:
    	
    Score();	
    ~Score();
    int getTeams();
    void setTeams(int n_teams); 
    int getGames();
    void setGames(int n_games);
    vector<Game> getMatches();
    void setMatches(vector<Game> matches);
    void readInputFile(char* fileName);
	vector<int> getOrderTeams();	
    int getIndex(int team);
    void simulateCompetition(int teams, int games);

};

#endif
