#include "Wp.h"
#include <vector>

Wp::Wp(){
}

Wp::~Wp(){
}

vector< double > Wp::calculateWP(Score score){
    vector< double > wins;    
    vector< double > games;
    vector< double > result;
    
    //init
    for(int i = 0; i < score.getTeams(); i++){
        wins.push_back(0);
        games.push_back(0);
        result.push_back(0);
    }     

    for(int game = 0; game < score.getGames();game++) {
        int teamA = score.getIndex(score.getMatches()[game].getTeamA());        
        int teamB = score.getIndex(score.getMatches()[game].getTeamB()); 	
        
        games[teamA]++;
        games[teamB]++;

        int goalsA = score.getMatches()[game].getGoalsA();
		int goalsB = score.getMatches()[game].getGoalsB();
		//Si gano equipo A
		if(goalsA > goalsB){
			wins[teamA]++;
		}else{
			wins[teamB]++;	
		}

        result[teamA] = wins[teamA] / games[teamA];
        result[teamB] = wins[teamB] / games[teamB];
    }

    //print
    //Print
	for(int i = 0; i < result.size(); i++){
		cout << result[i] << endl;
	}

    return result;

}
