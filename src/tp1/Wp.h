#ifndef WP_H
#define WP_H

#include "Score.h"

using namespace std;

class Wp {
	
    public:
		Wp();
		~Wp();	
		vector< double > calculateWP(Score score);
		
};

#endif
