#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <time.h>
#include "Score.h"
#include "Game.h" 
#include "Cmm.h"
#include "Wp.h"

using namespace std;

const int CMM_EG = 0;
const int CMM_CL = 1;
const int WP = 2;
const int CMM_EG_E = 3;
const int CMM_CL_E = 4;

void printResult(char* fileName, vector<double> result, Score score){
	ofstream outputFile;
	ofstream outputFile2;
	outputFile.open(fileName);
	std::string fileName2(fileName);
	fileName2 = "_"+fileName2;
	char *fn = &fileName2[0u];
	outputFile2.open(fn);
	for(int i = 0; i < result.size();i++){
		outputFile << result[i] << endl;
		outputFile2 << score.getOrderTeams()[i] << ' ' << result[i] << endl;
	}
	outputFile.close();
	outputFile2.close();
}

int main(int argc, char** argv) {

    Score score; 

    if(argc == 4 && argv[1] == std::string("simulator")){

        int n = atoi(argv[2]);
        int smethod = atoi(argv[3]);

        printf("#TEAMS  #GAMES TIME(sec)\n"); 
        for(int i = 2; i <= n ; i++){   
            int dim = (i-1)*i;     
            score.simulateCompetition(i,dim);
            Cmm cmm;
	        vector< vector<double> > colley_matrix;
            colley_matrix = cmm.createColleyMatrix(score);
            for(int j = 0; j < 11 ; j++){ 
                clock_t start, end; 
               	if(smethod == CMM_EG){	    
                      start = clock(); 
			          cmm.eg(colley_matrix, score.getTeams());
                      end = clock();
                }else{
                    start = clock();                      
                    cmm.cholesky(colley_matrix, score.getTeams());
			        end = clock();
                }
			    printf("%d %d %f\n", score.getTeams(), score.getGames(),((double) (end - start)) / CLOCKS_PER_SEC); 
            }
        }

    }else{  
    
	
	    if (argc < 4) {
            cerr << "Usage: " << argv[0] << " INPUTFILE " << "OUTPUTFILE " << "METHOD( 0 CMM-EG | 1 CMM-CL | 2 WP )" << endl;
            return 1;
        }
    
        score.readInputFile(argv[1]);
        /*
	    cout << "#TEAMS: " <<  score.getTeams() << endl;
	    cout << "#GAMES: " <<  score.getGames() << endl;
	
	    for(int game = 0; game < score.getGames(); game++) {
		    cout << score.getMatches()[game].getDate()  <<   " ";
		    cout << score.getMatches()[game].getTeamA()  <<  " ";
		    cout << score.getMatches()[game].getGoalsA() <<  " ";
		    cout << score.getMatches()[game].getTeamB()  <<  " ";
		    cout << score.getMatches()[game].getGoalsB() <<  endl;
	    }
	    cout <<  endl;
	    */
	    int method = atoi(argv[3]);
	
	    Cmm cmm;
	    vector< vector<double> > colley_matrix;
	    vector<double> result;
	    Wp wp;
	    clock_t start, end; 
	    switch ( method ) {
		    case CMM_EG:
			    cout << "Ejecutando CMM-EG" << endl;
			    colley_matrix = cmm.createColleyMatrix(score);
			    start = clock(); 
			    result = cmm.eg(colley_matrix, score.getTeams());
			    end = clock();
			    printf("The time was: %f\n", ((double) (end - start)) / CLOCKS_PER_SEC); 
			    cout << "Fin CMM-EG" << endl;
			    break;
		    case CMM_CL:
			    cout << "Ejecutando CMM-CL" << endl;
  			    colley_matrix = cmm.createColleyMatrix(score);
			    start = clock(); 
			    result = cmm.cholesky(colley_matrix, score.getTeams());
			    end = clock();
			    printf("The time was: %f\n", ((double) (end - start)) / CLOCKS_PER_SEC); 
			    cout << "Fin CMM-CL" << endl;
			    break;
		    case WP:
			    cout << "Ejecutando WP" << endl;
    		    result = wp.calculateWP(score);
    		    cout << "Fin WP" << endl;
			    break;
            case CMM_EG_E:
			    cout << "Ejecutando CMM-EG-E" << endl;
			    colley_matrix = cmm.createColleyMatrixWithDraw(score);
			    start = clock(); 
			    result = cmm.eg(colley_matrix, score.getTeams());
			    end = clock();
			    printf("The time was: %f\n", ((double) (end - start)) / CLOCKS_PER_SEC); 
			    cout << "Fin CMM-EG" << endl;
			    break;
		    case CMM_CL_E:
			    cout << "Ejecutando CMM-CL-E" << endl;
  			    colley_matrix = cmm.createColleyMatrixWithDraw(score);
			    start = clock(); 
			    result = cmm.cholesky(colley_matrix, score.getTeams());
			    end = clock();
			    printf("The time was: %f\n", ((double) (end - start)) / CLOCKS_PER_SEC); 
			    cout << "Fin CMM-CL" << endl;
			    break;
		    default:
  			    cerr << "METHOD: 0 CMM-EG | 1 CMM-CL | 2 WP" << endl;
        	    return 1;
  			    break;
	    }		
	
        printResult(argv[2], result, score);
    }
    return 0;
}


