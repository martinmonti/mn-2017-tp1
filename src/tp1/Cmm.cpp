#include "Cmm.h"
#include <vector>
#include <set>
#include <math.h> 

Cmm::Cmm(){
}

Cmm::~Cmm(){
}

vector< vector<double> > Cmm::createColleyMatrix(Score score) {
	
    //Init matrix
	vector< vector<double> > colley_matrix; 
	for(int i = 0; i < score.getTeams(); i++){
		vector<double> row;
		for(int j = 0; j < score.getTeams();j++){
			if(i == j){
				row.push_back(2);
			}else{
				row.push_back(0);
			}
		}
		row.push_back(1);
		colley_matrix.push_back(row);
	}
	
	//Ccarga de la colleyMatrix
	for(int game = 0; game < score.getGames();game++) {

        int teamA = score.getIndex(score.getMatches()[game].getTeamA());        
        int teamB = score.getIndex(score.getMatches()[game].getTeamB()); 	

        //diagonal 2+ni
        colley_matrix[teamA][teamA]++;
		colley_matrix[teamB][teamB]++;

        //-nji = -nij
		colley_matrix[teamA][teamB]--;
		colley_matrix[teamB][teamA]--;
		
        //bi = 1 + (wi - li) (1/2)
		int goalsA = score.getMatches()[game].getGoalsA();
		int goalsB = score.getMatches()[game].getGoalsB();
		//Si gano equipo A
		if(goalsA > goalsB){
			colley_matrix[teamA][score.getTeams()] = (colley_matrix[teamA][score.getTeams()]) + (0.5);
			colley_matrix[teamB][score.getTeams()] = colley_matrix[teamB][score.getTeams()] - (0.5);
		}else{
			colley_matrix[teamA][score.getTeams()] = colley_matrix[teamA][score.getTeams()] - (0.5);
			colley_matrix[teamB][score.getTeams()] = colley_matrix[teamB][score.getTeams()] + (0.5);	
		}
		
	}
	/*
    //Print
	/*for(int i = 0; i < score.getTeams(); i++){
		for(int j = 0; j < score.getTeams(); j++){
			cout << colley_matrix[i][j] << "  ";			
		}
		cout << "| " << colley_matrix[i][score.getTeams()]<< endl;
	}
    cout << endl;*/

    return colley_matrix;
	
}

vector<double> Cmm::eg(vector< vector<double> > matrix, int dim){
		
		//EG
		for(int z = 0; z < dim-1; z++){
			for(int i = (z+1); i < dim; i++) {
				double pivote = (matrix[i][z]/matrix[z][z]);
				for(int j = z; j < dim+1;j++){
						matrix[i][j] = (matrix[i][j]) - (pivote*matrix[z][j]);
				}
			}
		}
		
		//Print
		/*cout << endl;	
		for(int i = 0; i < dim; i++){
			for(int j = 0; j < dim; j++){
				cout << matrix[i][j] << "  ";			
			}
			cout << "| " << matrix[i][dim]<< endl;
		}*/
	
		return resolverTriangularSuperior(matrix, dim);		
}


vector<double> Cmm::cholesky(vector< vector<double> > matrix, int dim){
	
		vector< vector<double> > U;
		
		for(int i = 0; i < dim; i++){
			vector<double> row;
			for(int j = 0; j < dim;j++){
				row.push_back(0);
			}
			U.push_back(row);
		}
			
		for(int i = 0; i < dim ; i++){
			double diag = matrix[i][i];
			for(int z = 0; z < i ; z++){
				//diag = diag - pow(U[z][i],2);
				diag = diag - pow(matrix[z][i],2);
			}
			//U[i][i] = sqrt(diag);
			matrix[i][i] = sqrt(diag);
			for(int j = i+1; j< dim; j++){
				double value = matrix[i][j];
				for(int z = 0; z < i  ; z++){
					//value = value - U[z][i]*U[z][j];
					value = value - matrix[z][i]*matrix[z][j];
				}
				//U[i][j] = (value/U[i][i]);
				matrix[i][j] = (value/matrix[i][i]);
				matrix[j][i] = matrix[i][j];
			}	
		}
		
		//Print
		/*cout << endl;	
		for(int i = 0; i < dim; i++){
			for(int j = 0; j < dim; j++){
				//cout << U[i][j] << "  ";	
				cout << matrix[i][j] << "  ";		
			}
			cout << endl;
			//cout << "| " << matrix[i][dim]<< endl;
		}*/
				
		vector<double> resultado = resolverTriangularInferior(matrix, dim);
		resultado = resolverTriangularSuperior(matrix, dim, resultado);
		
		return resultado;		
}

vector<double> Cmm::resolverTriangularSuperior(vector< vector<double> > matrix, int dim){
		
		vector<double> resultado;
		
		//init
		for(int i = 0; i < dim; i++){
			resultado.push_back(matrix[i][dim]);
		}
	
		return resolverTriangularSuperior(matrix, dim, resultado);	
}

vector<double> Cmm::resolverTriangularSuperior(vector< vector<double> > matrix, int dim, vector<double> resultado){
		
	
		//Se resuelve el sistema de ecuaciones
		for(int i = dim-1; i >= 0; i--){
			for(int j = dim-1; j >= i; j--){
				if(i!=j){
					resultado[i] = resultado[i] - (matrix[i][j]*resultado[j]);
				}else{
					resultado[i] = resultado[i] / matrix[i][j];
				}				 	
			}
		}
		
		//print
		/*cout << endl;
		for(int i = 0; i < dim; i++){
			cout << resultado[i] << endl;
		}
		cout << endl;*/
	
		return resultado;		
}


vector<double> Cmm::resolverTriangularInferior(vector< vector<double> > matrix, int dim){
		
		vector<double> resultado;
		
		//init
		for(int i = 0; i < dim; i++){
			resultado.push_back(matrix[i][dim]);
		}
		
		//Se resuelve el sistema de ecuaciones
		for(int i = 0; i < dim ; i++){
			for(int j = 0; j <= i; j++){
				if(i!=j){
					resultado[i] = resultado[i] - (matrix[i][j]*resultado[j]);
				}else{
					resultado[i] = resultado[i] / matrix[i][j];
				}				 	
			}
		}
		
		//print
		/*cout << endl;
		for(int i = 0; i < dim; i++){
			cout << resultado[i] << endl;
		}
		cout << endl;*/
	
		return resultado;		
}

vector< vector<double> > Cmm::createColleyMatrixWithDraw(Score score) {
	
    //Init matrix
	vector< vector<double> > colley_matrix; 
	for(int i = 0; i < score.getTeams(); i++){
		vector<double> row;
		for(int j = 0; j < score.getTeams();j++){
			if(i == j){
				row.push_back(2);
			}else{
				row.push_back(0);
			}
		}
		row.push_back(1);
		colley_matrix.push_back(row);
	}
	
	//Ccarga de la colleyMatrix
	for(int game = 0; game < score.getGames();game++) {

        int teamA = score.getIndex(score.getMatches()[game].getTeamA());        
        int teamB = score.getIndex(score.getMatches()[game].getTeamB()); 	

        //diagonal 2+ni
        colley_matrix[teamA][teamA]++;
		colley_matrix[teamB][teamB]++;

        //-nji = -nij
		colley_matrix[teamA][teamB]--;
		colley_matrix[teamB][teamA]--;
		
        //bi = 1 + (wi - li) (1/2)
		int goalsA = score.getMatches()[game].getGoalsA();
		int goalsB = score.getMatches()[game].getGoalsB();
		//Si gano equipo A
		if(goalsA > goalsB){
			colley_matrix[teamA][score.getTeams()] = (colley_matrix[teamA][score.getTeams()]) + (0.5);
			colley_matrix[teamB][score.getTeams()] = colley_matrix[teamB][score.getTeams()] - (0.5);
		}else{
            if(goalsA < goalsB){
			    colley_matrix[teamA][score.getTeams()] = colley_matrix[teamA][score.getTeams()] - (0.5);
			    colley_matrix[teamB][score.getTeams()] = colley_matrix[teamB][score.getTeams()] + (0.5);	
            }
	    }
		
	}
	
    //Print
	for(int i = 0; i < score.getTeams(); i++){
		for(int j = 0; j < score.getTeams(); j++){
			cout << colley_matrix[i][j] << "  ";			
		}
		cout << "| " << colley_matrix[i][score.getTeams()]<< endl;
	}
    cout << endl;

    return colley_matrix;
	
}
