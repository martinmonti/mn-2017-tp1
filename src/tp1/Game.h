#ifndef Game_H
#define Game_H

#include <iostream>

using namespace std;

class Game {
	
	private:
    
    string _date;
    int _team_a;
    int _goals_a;
    int _team_b;
    int _goals_b;
    
    public:
    	
    Game();	
    ~Game();
    string getDate();
    void setDate(string date); 
    int getTeamA();
    void setTeamA(int teamA);
	int getGoalsA();
    void setGoalsA(int goalsA);
	int getTeamB();
    void setTeamB(int teamB);
	int getGoalsB();
    void setGoalsB(int goalsB);	

};

#endif
