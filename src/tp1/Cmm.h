#ifndef CMM_H
#define CMM_H

#include "Score.h"

using namespace std;

class Cmm {
	
    public:
		Cmm();
		~Cmm();	
		vector< vector<double> > createColleyMatrix(Score score);
		vector<double> eg(vector< vector<double> > matrix, int dim); 
		vector<double> cholesky(vector< vector<double> > matrix, int dim);
		vector<double> resolverTriangularSuperior(vector< vector<double> > matrix, int dim, vector<double> resultado);
		vector<double> resolverTriangularSuperior(vector< vector<double> > matrix, int dim);
		vector<double> resolverTriangularInferior(vector< vector<double> > matrix, int dim);
        vector< vector<double> > createColleyMatrixWithDraw(Score score);
};
#endif
