#include "Game.h"

Game::Game(){
}

Game::~Game(){
}
    
string Game::getDate(){
	return _date;
}

void Game::setDate(string date){
	_date = date;
}
 
int Game::getTeamA(){
	return _team_a;
}

void Game::setTeamA(int teamA){
	_team_a = teamA;
}

int Game::getGoalsA(){
	return _goals_a;
}

void Game::setGoalsA(int goalsA){
	_goals_a = goalsA;
}

int Game::getTeamB(){
	return _team_b;
}

void Game::setTeamB(int teamB){
	_team_b = teamB;
}

int Game::getGoalsB(){
	return _goals_b;
}

void Game::setGoalsB(int goalsB){
	_goals_b = goalsB;
}
