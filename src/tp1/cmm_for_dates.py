import time
import sys
import os
import matplotlib.pyplot as plt
import matplotlib.dates as pltdates
from random import randint
from sets import Set
import datetime

def main(argv):
	file = open(argv[2])
	dates = getDates(file.readlines())
	print len(dates)
	sorted_dates = sorted(list(dates))
	#for date in sorted_dates:
	#	os.system("python test_cmm.py " + argv[1] + ' ' + argv[2] + ' ' + ' ./output/experimento/CMvsCP_tiempo/'+ argv[3] + date + ' ' + date)
	#	print "python test_cmm.py " + argv[1] + ' ' + argv[2] + ' ' + ' ./output/'+ argv[3] + date + ' ' + date
	teams = open(argv[1]).readline().split(' ')
	mapCMM = {}
	mapCP = {}
	for team in teams:
		listCMM = []
		listCP = []
		for date in sorted_dates:
			output_file = open('./output/experimento/CMvsCP_tiempo/'+ argv[3] + date)
			for line in output_file.readlines():
				line = line.strip('\n').split(' ')
				if  line[0] == team:
					listCMM.append(line[1])
					listCP.append(line[2])
					break
		mapCMM[team] = listCMM
		mapCP[team] = listCP
	#random = randint(0,len(teams)-1)
	print teams
	random = teams.index('123921')
	#print random
	#print mapCP[teams[random]]
	
	#print mapCMM[teams[random]]
	#plt.style.use('ggplot')
	print [datetime.datetime.strptime(date, "%Y%m%d").strftime("%Y-%m-%d") for date in sorted_dates]
	print mapCMM[teams[random]]
	print mapCP[teams[random]]
	#dates = [time.strptime(i, '%Y%m%d') for i in sorted_dates]	
	#plt.plot(sorted_dates, mapCMM[teams[random]], label ='CMM', linewidth=10)
	#plt.show()	
	#plt.plot(sorted_dates, mapCP[teams[random]], label='CP', linewidth=10)
	#plt.ylabel('Numero que representa su posicion en una tabla')
	
	#plt.xlabel('2015 YYMMDD \n Tiempo \n Hasta que fecha se toma en cuenta')
	#plt.title('Diferencia en comportamiento de WP y CMM')
	#plt.show()
	#print mapCMM
	
	#print mapCP	

	


def getDates(lines):
	res = Set()
	for line in lines[1:]:
		posibleDate = line.strip('\n').split(' ')[0]
		if len(posibleDate) == 8:
			res.add(posibleDate)
	return res
			
if __name__ == "__main__":
    main(sys.argv)	
