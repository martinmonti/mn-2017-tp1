import numpy as np
import sys
import time
from scipy.linalg import solve




def main(argv):
	#print argv
	if len(argv) < 4:
		print 'test_cmm.py team_file matches_file output_file'
		return 
	before_date = None
	extra_params = None
	#list of the teams file
	teams_file = open(argv[1],'r')
	teams = [int(i) for i in teams_file.readline().strip('\n').split(' ')]
	teams_file.close()
	#matches file
	matches_file = open(argv[2],'r')
	dim = int(matches_file.readline().strip('\n').split(' ')[0])	
	lines = matches_file.readlines()
	#print lines
	#output file
	output_file = open(argv[3], 'w')
	if len(argv) == 5:
		if len(argv[4]) == 8:
			#before_date
			before_date = time.strptime(argv[4], '%Y%m%d')
		else:
			extra_params_file = open(argv[4])
			extra_params = extra_params_file.readlines()
			#print extra_params

	CMM_matrix = CMM(teams, dim, before_date, extra_params)
	CMM_matrix.make_A_matrix(lines)

	if CMM_matrix.extra_params is not None:
		CMM_matrix.add_extra_team()

	CMM_matrix.make_b_matrix()	
	#result is in A_matrix	
	gaussian_elimination(CMM_matrix.A_matrix, CMM_matrix.b_matrix, CMM_matrix.dim)
	#result is in b_matrix
	resolve_equation(CMM_matrix.A_matrix, CMM_matrix.b_matrix, CMM_matrix.dim)
	#order for the number of the team
	#order_result(CMM_matrix.b_matrix, CMM_matrix.positions, CMM_matrix.dim)
	i = 0
	for xi in CMM_matrix.b_matrix:
		try:
			WP = float(CMM_matrix.w_position[i]) / (CMM_matrix.w_position[i] + CMM_matrix.l_position[i])
		except ZeroDivisionError:
			WP = 0 	
		output_file.write(str(CMM_matrix.positions[i]) + ' ' +str(xi) + ' ' + str(WP) + '\n')
		i+=1


def resolve_equation(A_matrix, b_matrix, dim):
	for i in range(dim-1, -1, -1):
		for j in range(dim-1, -1, -1):
			if i == j :
				b_matrix[i] = b_matrix[i] / A_matrix[i][j]
			else : 
				b_matrix[i] -= A_matrix[i][j] * b_matrix[j]

def order_result(result, positions, dim):
	for i in range(0, dim):
		j =  positions.index(min(positions[i:]))
		result[j], result[i] = result[i], result[j]
		positions[j], positions[i] = positions[i], positions[j]

def gaussian_elimination(A_matrix, b_matrix, dim) :

	for i in range(0, dim-1):
		for j in range(i+1,dim):
			pivote = float(A_matrix[j][i]) / A_matrix[i][i]
			for k in range(i,dim):
				A_matrix[j][k] -= pivote*A_matrix[i][k]
			b_matrix[j]-= pivote*b_matrix[i]	

class CMM:
	def __init__(self, teams, n, before_date=None, extra_params=None):
		self.dim = n
		self.A_matrix = [[0]* n for i in range(n)]
		self.b_matrix = [0] * n
		self.positions = teams
		self.w_position = [0] * n
		self.l_position = [0] * n
		self.before_date = before_date
		self.extra_params = extra_params

	def add_extra_team(self):
		self.positions.append(max(self.positions) + 1)
		self.w_position.append(0)
		self.l_position.append(0)
		self.b_matrix.append(0)
		for row in self.A_matrix:
			row.append(0)
		self.dim += 1
		self.A_matrix.append([0] * self.dim )
		#print self.extra_params
		self.make_A_matrix(self.extra_params)


	

	def make_b_matrix(self):		
		for i in range(self.dim) :
			l = float(self.l_position[i])
			w = float(self.w_position[i])
			self.A_matrix[i][i] += 2
			self.b_matrix[i] = 1.0 + ((w - l) / 2.0)


	def make_A_matrix(self, lines):
		#print self.positions
		#print lines
		for line in lines:
			#print line
			split = line.strip('\n').split(' ')
			if len(split[0]) == 8 and self.before_date is not None:
				#date of the match
				match_time = time.strptime(split[0], '%Y%m%d')
				if match_time > self.before_date:
					#print 'grande'
					continue
			#print split[0]		
			a = int(split[1])
			b = int(split[3])		
			#team a position
			t_a = self.positions.index(a)
			#team b position
			t_b = self.positions.index(b)
		
			#team a result
			t_a_r = int(split[2])
			#team b result
			t_b_r = int(split[4])
			if t_a_r > t_b_r:
				self.w_position[t_a] += 1
				self.l_position[t_b] += 1
			else :
				self.w_position[t_b] += 1
				self.l_position[t_a] += 1
			self.A_matrix[t_a][t_a] += 1	
			self.A_matrix[t_b][t_b] += 1
			self.A_matrix[t_b][t_a] -= 1
			self.A_matrix[t_a][t_b] -= 1
		#print self.A_matrix		


	
if __name__ == "__main__":
    main(sys.argv)	

