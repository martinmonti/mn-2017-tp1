\select@language {spanish}
\contentsline {section}{\numberline {1}Resumen}{2}
\contentsline {section}{\numberline {2}Colley's Matrix Method}{3}
\contentsline {subsection}{\numberline {2.1}Introducci\IeC {\'o}n al m\IeC {\'e}todo}{3}
\contentsline {subsection}{\numberline {2.2}Explicaci\IeC {\'o}n te\IeC {\'o}rica del m\IeC {\'e}todo}{3}
\contentsline {subsubsection}{\numberline {2.2.1}Estad\IeC {\'\i }stica basica}{3}
\contentsline {subsubsection}{\numberline {2.2.2}Stregh of Schedule(SOS)}{4}
\contentsline {section}{\numberline {3}Desarrollo}{5}
\contentsline {section}{\numberline {4}Experimentaci\IeC {\'o}n}{6}
\contentsline {subsection}{\numberline {4.1}Comparaci\IeC {\'o}n cualitativa}{6}
\contentsline {subsubsection}{\numberline {4.1.1}Elecci\IeC {\'o}n del m\IeC {\'e}todo alternativo}{6}
\contentsline {subsubsection}{\numberline {4.1.2}Existencia de puntaje insuperable}{6}
\contentsline {subsubsection}{\numberline {4.1.3}Peso de los rivales enfrentados}{6}
\contentsline {subsubsection}{\numberline {4.1.4}Peso de los rivales vencidos}{8}
\contentsline {paragraph}{Peque\IeC {\~n}a discusi\IeC {\'o}n sobre el valor de los rivales vencidos.}{9}
\contentsline {subsubsection}{\numberline {4.1.5}Experimento CMM vs WP durante el tiempo }{10}
\contentsline {paragraph}{Hipotesis:}{10}
\contentsline {paragraph}{Resultados:}{10}
\contentsline {paragraph}{Conclusi\IeC {\'o}n:}{11}
\contentsline {subsubsection}{\numberline {4.1.6}Experimento Mejor estrategia con menor cantidad de partidos ganados }{11}
\contentsline {paragraph}{Hipotesis:}{11}
\contentsline {paragraph}{Resultados:}{12}
\contentsline {paragraph}{Conclusiones:}{13}
\contentsline {subparagraph}{Caso donde nuestra estrategia es mejor}{13}
\contentsline {subsection}{\numberline {4.2}Comparaci\IeC {\'o}n cuantitavia}{14}
\contentsline {subsubsection}{\numberline {4.2.1}Comparativa entre Eliminaci\IeC {\'o}n Gaussiana y Factorizaci\IeC {\'o}n de Cholesky}{15}
\contentsline {paragraph}{Desarrollo del experimento}{15}
\contentsline {paragraph}{Hipotesis}{15}
\contentsline {paragraph}{Conclusi\IeC {\'o}n del experimento}{16}
\contentsline {subsection}{\numberline {4.3}Que pasar\IeC {\'\i }a si tomaramos a los empates en cuenta}{17}
\contentsline {subsubsection}{\numberline {4.3.1}Nuestra soluci\IeC {\'o}n}{17}
\contentsline {subsubsection}{\numberline {4.3.2}Ejemplo practico y vision de resultados}{17}
\contentsline {section}{\numberline {5}Conclusiones generales}{18}
\contentsline {paragraph}{Aspecto cuantitativo:}{18}
\contentsline {paragraph}{Aspecto cualitativo:}{18}
\contentsline {section}{\numberline {6}Ap\IeC {\'e}ndice A - Enunciado}{19}
\contentsline {section}{\numberline {7}Ap\'endice B - Instrucciones de Compilaci\'on y Ejecuci\'on}{25}
\contentsline {section}{\numberline {8}Ap\'endice C - Bibliograf\'ia}{26}
